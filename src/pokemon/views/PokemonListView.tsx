import {FunctionComponent} from "react";
import {Pokemon} from "../domain/Pokemon";
import PokemonCard from "./PokemonCard";

interface PokemonListViewProps {
    pkmList: Array<Pokemon>;
    loading: boolean;
}

const PokemonListView: FunctionComponent<PokemonListViewProps> = (
    {
        pkmList,
        loading
    }
) => {

    if (loading)
        return <span>Loading...</span>

    if (pkmList)
        return pkmList.map(pkm => ( <PokemonCard pkm={pkm}/> ));

    return null;
};

export default PokemonListView;
