import {Name} from "./objectvalue/Name";
import {Item} from "./objectvalue/Item";
import {Level} from "./objectvalue/Level";
import {Nature} from "./objectvalue/Nature";
import {Types} from "./objectvalue/Types";
import {Gender} from "./objectvalue/Gender";

export class Pokemon {
    private _name: Name;
    private _item: Item;
    private _level: Level;
    private _nature: Nature;
    private _types: Types;
    private _gender: Gender;

    public constructor(
        name: Name,
        item: Item,
        level: Level,
        nature: Nature,
        types: Types,
        gender: Gender
    ) {
        this._name = name;
        this._item = item;
        this._level = level;
        this._nature = nature;
        this._types = types;
        this._gender = gender;
    }

    public getName(): string {
        return this._name.name;
    }

    public getItem(): string | undefined {
        return this._item.item;
    }

    public getLevel(): number {
        return this._level.level;
    }

    public getNature(): string {
        return this._nature.nature;
    }

    public getTypes(): Array<string> {
        return this._types.pkmType;
    }

    public getGender(): string | undefined {
        return this._gender.gender;
    }

    // No tenemos setter's

    public increaseLevel(): void {
        this._level = new Level(this._level.level + 1);
    }
}
