import {PokemonDTO} from "../dto/PokemonDTO";
import {Pokemon} from "../Pokemon";
import {Name} from "../objectvalue/Name";
import {Item} from "../objectvalue/Item";
import {Level} from "../objectvalue/Level";
import {Nature} from "../objectvalue/Nature";
import {Types} from "../objectvalue/Types";
import {Gender} from "../objectvalue/Gender";

export class PokemonMapper {

    public static fromDomainModel(pokemon: Pokemon): PokemonDTO {
        return {
            name: pokemon.getName(),
            item: pokemon.getItem(),
            level: pokemon.getLevel(),
            nature: pokemon.getNature(),
            types: pokemon.getTypes(),
            gender: pokemon.getGender()
        }
    }

    public static fromDTO(pokemon: PokemonDTO): Pokemon {
        return new Pokemon(
            new Name(pokemon.name),
            new Item(pokemon.item),
            new Level(pokemon.level),
            new Nature(pokemon.nature),
            new Types(pokemon.types),
            new Gender(pokemon.gender)
        );
    }
}
