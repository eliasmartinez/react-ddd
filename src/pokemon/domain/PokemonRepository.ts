import {Pokemon} from "./Pokemon";

export interface PokemonRepository {
    addPokemon: (newPokemon: Pokemon) => void;
    deletePokemon: (id: string) => void;
    fetchPokemonList: () => Promise<Array<Pokemon>>;
}
