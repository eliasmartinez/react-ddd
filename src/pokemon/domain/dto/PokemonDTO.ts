export interface PokemonDTO {
    name: string;
    item?: string;
    level: number;
    nature: string;
    types: Array<string>;
    gender?: string;
}
