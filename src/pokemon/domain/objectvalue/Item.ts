export class Item {
    private _item?: string;

    public constructor(item?: string) {
        this.ensureItemProvidedIsValid(item);

        this._item = item;
    }

    public get item(): string | undefined {
        return this._item;
    }

    private ensureItemProvidedIsValid(item?: string): void {
        if (item && !pkmItemType.includes(item))
            throw new Error(`Item: Invalid item provided`);
    }
}

export const pkmItemType = [
    'berry',
    'ball',
    'medical',
    'battle',
    undefined
];
