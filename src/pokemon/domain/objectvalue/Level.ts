export class Level {
    private _level: number;

    public constructor(pkmLevel: number) {
        this.ensurePkmLevelParameterIsNotUndefined(pkmLevel);
        this.ensurePkmLevelIsNotEqualsOrLowerThanZero(pkmLevel);
        this.ensurePkmLevelIsNotGreaterThanOneHundred(pkmLevel);

        this._level = pkmLevel;
    }

    private ensurePkmLevelParameterIsNotUndefined(pkmLevel: number): void {
        if (!pkmLevel)
            throw new Error("Level: pkmLevel property cannot be undefined");
    }

    private ensurePkmLevelIsNotEqualsOrLowerThanZero(pkmLevel: number): void {
        if (pkmLevel <= 0)
            throw new Error("Level: pkmLevel can not be equal or lower than cero");
    }

    private ensurePkmLevelIsNotGreaterThanOneHundred(pkmLevel: number): void {
        if (pkmLevel > 100)
            throw new Error("Level: pkmLevel exceed max level allowed");
    }

    public get level(): number {
        return this._level;
    }
}
