export class Types {

    private readonly _types: Array<string>;

    public constructor(types: Array<string>) {
        this.ensureTypesParameterIsNotUndefined(types);
        this.ensureItHasAtLestOneType(types);
        this.ensureItDoesNotHasMoreThanTwoTypes(types);
        this.ensureProvidedTypesAreValid(types);

        this._types = types;
    }

    public get pkmType(): Array<string> {
        return this._types;
    }

    private ensureTypesParameterIsNotUndefined(types: Array<string>): void {
        if (!types)
            throw new Error("Types: types property cannot be undefined");
    }

    private ensureItHasAtLestOneType(types: Array<string>): void {
        if (Array.isArray(types) && types.length === 0)
            throw new Error("Types: types property length is 0, must provided at least one type");
    }

    private ensureItDoesNotHasMoreThanTwoTypes(types: Array<string>): void {
        if (Array.isArray(types) && types.length > 2)
            throw new Error(`Types: types property length is ${types.length}, max types allowed are 2`);
    }

    private ensureProvidedTypesAreValid(types: Array<string>): void {
        types.map(type => {
            if (!pkmValidTypes.includes(type))
                throw new Error(`Types: Invalid type provided`);
        });
    }
}

const pkmValidTypes = [
    'bug',
    'dark',
    'dragon',
    'electric',
    'fairy',
    'fighting',
    'fire',
    'flying',
    'ghost',
    'grass',
    'ground',
    'ice',
    'normal',
    'poison',
    'psychic',
    'rock',
    'steel',
    'water'
];
