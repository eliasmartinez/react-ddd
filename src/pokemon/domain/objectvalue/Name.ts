export class Name {
    private _name: string;

    public constructor(name: string) {
        this.ensurePkmNameParameterIsNotUndefined(name);
        this.ensurePkmNameNotExceedMaxLengthAllowed(name);

        this._name = name;
    }

    private ensurePkmNameParameterIsNotUndefined(name: string): void {
        if (!name)
            throw new Error("Name: name property cannot be undefined");
    }

    private ensurePkmNameNotExceedMaxLengthAllowed(name: string): void {
        if (name.length > 12)
            throw new Error(`Name: name property length is ${name.length}, max length allowed is 12`);
    }

    public get name(): string {
        return this._name;
    }
}
