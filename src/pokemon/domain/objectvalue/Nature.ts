export class Nature {
    private _nature: string;

    public constructor(nature: string) {
        this.ensureNatureParameterIsNotUndefined(nature);
        this.ensureProvidedNatureIsValid(nature);

        this._nature = nature;
    }

    public get nature(): string {
        return this._nature;
    }

    private ensureNatureParameterIsNotUndefined(nature: string): void {
        if (!nature)
            throw new Error("PkmNature: nature property cannot be undefined");
    }

    private ensureProvidedNatureIsValid(nature: string): void {
        if (!pkmNatureType.includes(nature))
            throw new Error(`Nature: Invalid nature provided`);
    }
}

export const pkmNatureType = [
    'hardy',
    'calm',
    'rash',
    'brave',
    'serious',
    'hasty',
    'lonely'
];
