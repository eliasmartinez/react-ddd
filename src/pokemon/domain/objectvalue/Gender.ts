export class Gender {
    private _gender: string | undefined;

    public constructor(gender?: string) {
        this.ensureProvidedGenderIsValid(gender);

        this._gender = gender;
    }

    public get gender(): string | undefined {
        return this._gender;
    }

    private ensureProvidedGenderIsValid(gender?: string): void {
        if (gender && !pkmGenderTypes.includes(gender))
            throw new Error(`Gender: Invalid gender provided`);
    }
}

export const pkmGenderTypes = [
    'male',
    'female',
    undefined
];
