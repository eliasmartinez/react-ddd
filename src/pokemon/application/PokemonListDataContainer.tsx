import {FunctionComponent, useEffect, useState} from "react";
import {PokemonRepository} from "../domain/PokemonRepository";
import {Pokemon} from "../domain/Pokemon";
import PokemonListView from "../views/PokemonListView";

interface PokemonListDataContainerProps {
    pkmRepository: PokemonRepository
}

const PokemonListDataContainer: FunctionComponent<PokemonListDataContainerProps> = (
    {
        pkmRepository
    }
) => {

    const [pkmList, setPkmList] = useState<Array<Pokemon>>([]);
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        loadPokemon();
    });

    const loadPokemon = async () => {
        setLoading(true);
        pkmRepository.fetchPokemonList()
            .then(pkmList => setPkmList(pkmList))
            .catch(err => alert(err))
            .finally(() => setLoading(false))
    };

    return (
        <PokemonListView
            pkmList={pkmList}
            loading={loading}
        />
    );
};

export default PokemonListDataContainer;
