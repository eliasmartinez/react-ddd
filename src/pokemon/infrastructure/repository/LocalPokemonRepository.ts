import {PokemonRepository} from "../../domain/PokemonRepository";
import {Pokemon} from "../../domain/Pokemon";
import {PokemonDTO} from "../../domain/dto/PokemonDTO";
import {PokemonMapper} from "../../domain/mapper/PokemonMapper";

export class LocalPokemonRepository implements PokemonRepository {
    addPokemon(newPokemon: Pokemon): void {
        // to implement
    }

    deletePokemon(id: string): void {
        // to implement
    }

    fetchPokemonList(): Promise<Array<Pokemon>> {
        return new Promise<Array<Pokemon>>(() => {
            return pokemonMocks.map(pokemon => ( PokemonMapper.fromDTO(pokemon) ));
        });
    }
}

const pokemonMocks: Array<PokemonDTO> = [
    {
        name: "squirtle",
        level: 5,
        gender: "male",
        nature: "serious",
        types: ["water"]
    },
    {
        name: "bulbasaur",
        level: 5,
        gender: "female",
        nature: "calm",
        types: ["grass", "posion"]
    },
    {
        name: "metagross",
        level: 50,
        nature: "docil",
        item: "berry",
        types: ["steel", "psychic"]
    }
];
