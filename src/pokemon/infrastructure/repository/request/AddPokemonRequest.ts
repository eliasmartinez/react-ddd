import {Pokemon} from "../../../domain/Pokemon";
import {PokemonMapper} from "../../../domain/mapper/PokemonMapper";
import {PokemonDTO} from "../../../domain/dto/PokemonDTO";
import axios from "../../../../task/axios";

type AddPokemonRequest = PokemonDTO;

const addPokemonRequest = (pokemon: Pokemon) => {
    const newPokemon: AddPokemonRequest = PokemonMapper.fromDomainModel(pokemon);
    return axios.post<AddPokemonRequest>('/add', newPokemon);
};

export default addPokemonRequest;
