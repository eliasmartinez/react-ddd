import axios from "../../../../task/axios";

export type DeletePokemonRequest = {id: string};

const deletePokemonRequest = (deleteRequest: DeletePokemonRequest) => {
    return axios.post<DeletePokemonRequest>('/delete', deleteRequest);
};

export default deletePokemonRequest;
