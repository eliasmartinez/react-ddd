import axios from "../../../../task/axios";
import {PokemonDTO} from "../../../domain/dto/PokemonDTO";
import {Pokemon} from "../../../domain/Pokemon";
import {PokemonMapper} from "../../../domain/mapper/PokemonMapper";

const fetchPokemonListRequest = async (): Promise<Array<Pokemon>> => {
    const pokemonList: Array<PokemonDTO> = await axios.get<{}, Array<PokemonDTO>>('/list');
    return pokemonList.map((pokemon: PokemonDTO) => ( PokemonMapper.fromDTO(pokemon) ));
};

export default fetchPokemonListRequest;
