import {PokemonRepository} from "../../domain/PokemonRepository";
import {Pokemon} from "../../domain/Pokemon";
import addPokemonRequest from "./request/AddPokemonRequest";
import deletePokemonRequest from "./request/DeletePokemonRequest";
import fetchPokemonListRequest from "./request/FetchPokemonListRequest";

export class ApiPokemonRepository implements PokemonRepository {

    async addPokemon(newPokemon: Pokemon){
        return addPokemonRequest(newPokemon);
    }

    async deletePokemon(id: string){
        return deletePokemonRequest({id});
    }

    async fetchPokemonList(): Promise<Array<Pokemon>> {
        return fetchPokemonListRequest();
    }
}
