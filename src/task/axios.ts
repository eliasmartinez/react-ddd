import axios from "axios";

export default axios.create({
    baseURL: 'http://api.pokemon.v1',
    headers: {
        'Content-Type': 'application/json',
    }
})
